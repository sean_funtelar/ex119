﻿using System;

namespace week3_ex19
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine($"Question A | (6 + 7) * (3 - 2) = {(6 + 7) * (3 - 2)}");
            Console.WriteLine($"Question B | (6 * 7) + (3 * 2) =  {(6 * 7) + (3 * 2)}");
            Console.WriteLine($"Question C | (6 * 7) + 3 * 2 =  {(6 * 7) + 3 * 2}");
            Console.WriteLine($"Question D | (3 * 2) + 6 * 7 =  {(3 * 2) + 6 * 7}");
            Console.WriteLine($"Question E | (3 * 2) + 7 * 6 / 2 =  {(3 * 2) + 7 * 6 / 2}");
            Console.WriteLine($"Question F | 6 + 7 * 3 - 2 =  {6 + 7 * 3 - 2}");
            Console.WriteLine($"Question G | 3 * 2 + (3 * 2) =  {3 * 2 + (3 * 2)}");
            Console.WriteLine($"Question H | (6 * 7) * 7 + 6 =  {(6 * 7) * 7 + 6}");
            Console.WriteLine($"Question I | (2 * 2) + 2 * 2 =  {(2 * 2) + 2 * 2}");
            Console.WriteLine($"Question J | 3 * 3 + (3 * 3) =  {3 * 3 + (3 * 3)}");
            Console.WriteLine($"Question K | (6 \xB2 + 7) * 3 + 2 =  {(Math.Pow(6,2) + 7) * 3 + 2}");
            Console.WriteLine($"Question L | (3 * 2) + 3 \xB2 * 2 =  {(3 * 2) + Math.Pow(3,2) * 2}");

        }
    }
}